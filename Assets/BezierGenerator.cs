﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BezierGenerator : MonoBehaviour {
	
	private Light cLight;

	//Generate noise map
	public Texture2D maskTexture;
	public int pixWidth;
	public int pixHeight;
	private Texture2D targetTex;
	private Color[] pix;
	private Renderer rend;


	//3D perlin
	private Vector3[] currentBezierPoints;

	//Perlin properties
	public Vector3 startPosition = Vector3.zero;
	public Vector3 endPosition = new Vector3(256, 0, 256);
	public Vector3 startTangent;
	public Vector3 endTangent;
	public int division = 30;
	public int outline = 3;


	void Start () {
		cLight = GetComponent(typeof(Light)) as Light;
		if (cLight == null)
		{
			Debug.LogWarning("AnimateCookieTexture: No light found on this gameObject", this);
			enabled = false;
		}

		rend = GetComponent<Renderer>();
		targetTex = new Texture2D(pixWidth, pixHeight);
		targetTex.wrapMode = TextureWrapMode.Clamp;
		pix = new Color[targetTex.width * targetTex.height];
		
	}
	void CalcCurve()
	{
		float y = 0;
		while (y < targetTex.height)
		{
			float x = 0;
			while (x < targetTex.width)
			{
				pix[(int)(y * targetTex.width + x)] = new Color(0, 0, 0, 0);
				x++;
			}
			y++;
		}
		currentBezierPoints = Handles.MakeBezierPoints(startPosition, endPosition, startTangent, endTangent, division);
		foreach(Vector3 v in currentBezierPoints)
		{
			//Color maskColor = maskTexture.GetPixel(Mathf.RoundToInt(x * maskTexture.width / noiseTex.width), Mathf.RoundToInt(y * maskTexture.height / noiseTex.height));
			//float alphaValue = sample;
			//if (maskColor.a < alphaValue) alphaValue = maskColor.a;

			int targetIndex = (int)(Mathf.RoundToInt(v.x) * targetTex.width + Mathf.RoundToInt(v.z));
			ChangeTargetPixel(targetIndex);
			if(outline > 0)
			{
				for(int i = 1; i < outline; i++)
				{
					targetIndex = (int)(Mathf.RoundToInt(v.x - i) * targetTex.width + Mathf.RoundToInt(v.z));
					ChangeTargetPixel(targetIndex, 1 - (float)i / outline);
					targetIndex = (int)(Mathf.RoundToInt(v.x + i) * targetTex.width + Mathf.RoundToInt(v.z));
					ChangeTargetPixel(targetIndex, 1 - (float)i / outline);
					//for (int j = 1; j < outline; j++)
					//{
					//	targetIndex = (int)(Mathf.RoundToInt(v.x - i) * targetTex.width + Mathf.RoundToInt(v.z - j));
					//	ChangeTargetPixel(targetIndex, 1 - (float)i / outline);
					//	targetIndex = (int)(Mathf.RoundToInt(v.x + i) * targetTex.width + Mathf.RoundToInt(v.z + j));
					//	ChangeTargetPixel(targetIndex, 1 - (float)i / outline);
					//}
				}
			}
		}

		targetTex.SetPixels(pix);
		targetTex.Apply();
	}

	void ChangeTargetPixel(int targetIndex, float alphaValue = 1)
	{
		if (targetIndex < pix.Length && targetIndex > 0)
		{
			pix[targetIndex] = new Color(1, 1, 1, alphaValue);
		}
	}
	void Update()
	{
		CalcCurve();
		cLight.cookie = targetTex;
	}

}
