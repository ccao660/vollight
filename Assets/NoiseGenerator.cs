﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LibNoise.Generator;
using LibNoise;

public class NoiseGenerator : MonoBehaviour {
	
	private Light cLight;

	//Generate noise map
	public Texture2D maskTexture;
	public int pixWidth;
	public int pixHeight;
	public float xOrg;
	public float yOrg;
	public float zOrg;
	public float scale = 1.0F;
	private Texture2D noiseTex;
	private Color[] pix;
	private Renderer rend;


	//3D perlin
	private Perlin perlin;

	//Perlin properties
	public double frequency = 0;
	public double lacunarity = 0;
	public int octaves = 0;
	public double persistence = 0;
	public int seed = 0;
	public QualityMode quality = QualityMode.Medium;

	void Start () {
		cLight = GetComponent(typeof(Light)) as Light;
		if (cLight == null)
		{
			Debug.LogWarning("AnimateCookieTexture: No light found on this gameObject", this);
			enabled = false;
		}

		rend = GetComponent<Renderer>();
		noiseTex = new Texture2D(pixWidth, pixHeight);
		noiseTex.wrapMode = TextureWrapMode.Clamp;
		pix = new Color[noiseTex.width * noiseTex.height];
		//Generate a perlin
		perlin = new Perlin(frequency, lacunarity, persistence, octaves, seed, quality);

	}
	void CalcNoise()
	{
		float y = 0;
		while (y < noiseTex.height)
		{
			float x = 0;
			while (x < noiseTex.width)
			{
				float xCoord = xOrg + x / noiseTex.width * scale;
				float yCoord = yOrg + y / noiseTex.height * scale;
				float sample = (float)perlin.GetValue(xCoord, yCoord, zOrg * scale);
				if (sample > 1)
				{
					print(sample);
				}
				Color maskColor = maskTexture.GetPixel(Mathf.RoundToInt(x * maskTexture.width / noiseTex.width), Mathf.RoundToInt(y * maskTexture.height / noiseTex.height));
				float alphaValue = sample;
				if (maskColor.a < alphaValue) alphaValue = maskColor.a;
				pix[(int)(y * noiseTex.width + x)] = new Color(sample, sample, sample, alphaValue);
				x++;
			}
			y++;
		}
		
		noiseTex.SetPixels(pix);
		noiseTex.Apply();
	}
	void Update()
	{
		CalcNoise();
		cLight.cookie = noiseTex;
	}

}
