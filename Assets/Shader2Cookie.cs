﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Shader2Cookie : MonoBehaviour {


	private Light cLight;

	//public Material Mat;

	public Texture2D OrginalTex;

	private Texture2D maskTexture;
	//public int pixWidth;
	//public int pixHeight;
	private Texture2D targetTex;

	public RenderTexture rendtex;



	// Use this for initialization
	void Start () {
		cLight = GetComponent(typeof(Light)) as Light;
		if (cLight == null)
		{
			Debug.LogWarning("AnimateCookieTexture: No light found on this gameObject", this);
			enabled = false;
		}


		//rendtex.wrapMode = TextureWrapMode.Clamp;







		targetTex = new Texture2D(rendtex.width, rendtex.height);
		targetTex.wrapMode = TextureWrapMode.Clamp;

		maskTexture =  new Texture2D(rendtex.width, rendtex.height);
		maskTexture.wrapMode = TextureWrapMode.Clamp;





		//Graphics.Blit (maskTexture, rendtex, Mat);

	}
	
	// Update is called once per frame
	void Update () {
		//Graphics.Blit (maskTexture, rendtex, Mat);
		//targetTex.ReadPixels(new Rect(0, 0, rendtex.width, rendtex.height), 0, 0);
		//RenderTexture currentActiveRT = RenderTexture.active;
		//RenderTexture.active = rendtex;

		RenderTexture.active = rendtex;


		OrginalTex.ReadPixels(new Rect(0, 0, rendtex.width, rendtex.height), 0, 0);

		OrginalTex.Apply();

		cLight.cookie = OrginalTex;
	}
}
