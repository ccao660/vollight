﻿Shader "ShaderToy/NewImageEffectShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}

	CGINCLUDE

	#include "UnityCG.cginc"
    sampler2D _MainTex;

	struct appdata_t
	{
		float4 vertex : POSITION;
		float2 texcoord : TEXCOORD0;
	};

	struct v2f
	{
		float2 uv : TEXCOORD0;
		float4 vertex : SV_POSITION;
	};

	v2f vert (appdata_img v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.uv = v.texcoord;
		return o;
	}

	float circle(in float2 _st,in float _radius){
		float2 dist = _st-float2(0.5,0.5);
		return 1.0-smoothstep(_radius-(_radius*1.0),
                 			  _radius+(_radius*1.08),
                 			dot(dist,dist)*6.344);

	}

	float BlackCircle(in float2 _st,in float _radius){
		float2 dist = _st-float2(0.5,0.5);
		return smoothstep(_radius-(_radius*1.00),
                 			  _radius+(_radius*0.6),
                 			dot(dist,dist)*6.344)-1.0;

	}
	


	half4 frag (v2f i) : COLOR
	{
		float2 st = i.uv;
		float4 circleShape = float4 (circle(st,0.780)+BlackCircle(st,0.5),
									 circle(st,0.780)+BlackCircle(st,0.5),
									 circle(st,0.780)+BlackCircle(st,0.5),
									 circle(st,0.780)+BlackCircle(st,0.5)
									);
		
		
		
		
		
		half4 col = float4(1.0,1.0,1.0,1.0);
		//fixed4 col = tex2D(float4(1.0,1.0,1.0,1.0), i.uv);
		// just invert the colors


		col.rgba = circleShape;
		return col;
	}
	ENDCG

	SubShader
	{

		Pass
		{
		    // No culling or depth
			Cull Off 
			//ZWrite Off 
			ZTest Always

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			ENDCG


		}
	}
}
