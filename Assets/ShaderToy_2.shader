﻿Shader "ShaderToy/ShaderToy_2"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}

	CGINCLUDE

	#include "UnityCG.cginc"
    sampler2D _MainTex;

	struct appdata_t
	{
		float4 vertex : POSITION;
		float2 texcoord : TEXCOORD0;
	};

	struct v2f
	{
		float2 uv : TEXCOORD0;
		float4 vertex : SV_POSITION;
	};

	v2f vert (appdata_img v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.uv = v.texcoord;
		return o;
	}

	float plot(float2 st, float pct) {
		return smoothstep(pct - 0.02, pct, st.y) -
			smoothstep(pct, pct + 0.020, st.y);

	}
	/*float circle(in float2 _st,in float _radius){
		float2 dist = _st-float2(0.5,0.5);
		return 1.0-smoothstep(_radius-(_radius*1.0),
                 			  _radius+(_radius*1.08),
                 			dot(dist,dist)*6.344);

	*/

	/*float BlackCircle(in float2 _st,in float _radius){
		float2 dist = _st-float2(0.5,0.5);
		return smoothstep(_radius-(_radius*1.00),
                 			  _radius+(_radius*0.6),
                 			dot(dist,dist)*6.344)-1.0;

	}*/
	


	half4 frag (v2f i) : COLOR
	{
		float2 st = i.uv;
		float y = smoothstep(-0.024, 0.948, st.x) - smoothstep(0.284, 1.024, st.x);;
		y = sin(st.x*4.368+_Time*10.0)*0.316 + 0.508;
		float3 color = float3(y, y, y);
		float pct = plot(st, y);
		color = (0.096 - pct)*color + pct*float3(0.999, 0.978, 1.000);

		half4 FinalOutput = float4(1.0, 1.0, 1.0, 1.0);
		FinalOutput.a = color.r;
		return FinalOutput;
	}
	ENDCG

	SubShader
	{

		Pass
		{
		    // No culling or depth
			Cull Off 
			//ZWrite Off 
			ZTest Always

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			ENDCG


		}
	}
}
