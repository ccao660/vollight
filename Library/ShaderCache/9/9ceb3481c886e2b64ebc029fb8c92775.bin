��                       ;V  #ifdef VERTEX
#version 150
#extension GL_ARB_explicit_attrib_location : require
#extension GL_ARB_shader_bit_encoding : enable

uniform 	vec4 _ProjectionParams;
uniform 	vec4 hlslcc_mtx4x4VolumetricMVP[4];
in  vec4 in_POSITION0;
out vec4 vs_TEXCOORD0;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4VolumetricMVP[1];
    u_xlat0 = hlslcc_mtx4x4VolumetricMVP[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4VolumetricMVP[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4VolumetricMVP[3] * in_POSITION0.wwww + u_xlat0;
    gl_Position = u_xlat0;
    u_xlat0.y = u_xlat0.y * _ProjectionParams.x;
    u_xlat1.xzw = u_xlat0.xwy * vec3(0.5, 0.5, 0.5);
    vs_TEXCOORD0.zw = u_xlat0.zw;
    vs_TEXCOORD0.xy = u_xlat1.zz + u_xlat1.xw;
    return;
}

#endif
#ifdef FRAGMENT
#version 150
#extension GL_ARB_explicit_attrib_location : require
#extension GL_ARB_shader_bit_encoding : enable

uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _ProjectionParams;
uniform 	vec4 _ScreenParams;
uniform 	vec4 hlslcc_mtx4x4unity_CameraToWorld[4];
uniform 	vec4 _LightSplitsNear;
uniform 	vec4 _LightSplitsFar;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	vec4 Phase;
uniform 	vec4 Phase2;
uniform 	vec4 Density;
uniform 	vec4 hlslcc_mtx4x4hxVolumeMatrixOld[40];
uniform 	vec2 hxVolumeSettingsOld[10];
uniform 	float hxRayOffset;
uniform 	float HxTileSize;
uniform 	vec4 hlslcc_mtx4x4InverseProjectionMatrix[4];
uniform 	float FirstLight;
uniform 	vec3 ShadowBias;
uniform 	vec4 _SpotLightParams;
uniform 	vec3 CameraFoward;
uniform 	float VolumeScale;
uniform 	float ExtinctionEffect;
uniform 	vec3 LightColour;
uniform 	vec3 LightColour2;
uniform 	float TintPercent;
uniform 	vec2 MaxRayDistance;
uniform 	vec2 SunSize;
uniform  sampler2D VolumetricDepth;
uniform  sampler2D Tile5x5;
uniform  sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform  sampler2D _ShadowMapTexture;
in  vec4 vs_TEXCOORD0;
layout(location = 0) out vec4 SV_Target0;
vec4 u_xlat0;
vec4 u_xlat1;
vec2 u_xlat16_1;
vec4 u_xlat10_1;
vec4 u_xlat2;
int u_xlati3;
vec3 u_xlat4;
vec4 u_xlat10_4;
vec3 u_xlat5;
vec3 u_xlat6;
vec4 u_xlat7;
bvec4 u_xlatb7;
vec4 u_xlat8;
bvec4 u_xlatb8;
vec4 u_xlat9;
bvec4 u_xlatb9;
vec3 u_xlat10;
bvec4 u_xlatb10;
vec3 u_xlat11;
float u_xlat12;
bool u_xlatb12;
float u_xlat15;
bool u_xlatb15;
float u_xlat17;
vec3 u_xlat20;
int u_xlati20;
bool u_xlatb20;
float u_xlat21;
float u_xlat24;
bool u_xlatb24;
vec2 u_xlat27;
bvec2 u_xlatb27;
float u_xlat32;
bool u_xlatb32;
float u_xlat33;
float u_xlat36;
float u_xlat37;
int u_xlati37;
float u_xlat38;
float u_xlat16_38;
int u_xlati38;
bool u_xlatb38;
float u_xlat40;
int u_xlati40;
bool u_xlatb40;
float u_xlat41;
int u_xlati41;
bool u_xlatb41;
float u_xlat42;
bool u_xlatb42;
float u_xlat43;
float u_xlat16_43;
int u_xlati43;
bool u_xlatb43;
float u_xlat44;
float u_xlat45;
void main()
{
    u_xlat0.xy = vs_TEXCOORD0.xy / vs_TEXCOORD0.ww;
    u_xlat10_1 = textureLod(VolumetricDepth, u_xlat0.xy, 0.0);
    u_xlat24 = dot(u_xlat10_1.xy, vec2(1.0, 0.00392156886));
    u_xlat16_1.xy = u_xlat10_1.zw * vec2(2.0, 2.0) + vec2(-1.0, -1.0);
    u_xlat2 = u_xlat16_1.yyyy * hlslcc_mtx4x4InverseProjectionMatrix[1];
    u_xlat1 = hlslcc_mtx4x4InverseProjectionMatrix[0] * u_xlat16_1.xxxx + u_xlat2;
    u_xlat1 = u_xlat1 + hlslcc_mtx4x4InverseProjectionMatrix[2];
    u_xlat1 = u_xlat1 + hlslcc_mtx4x4InverseProjectionMatrix[3];
    u_xlat1.xyz = u_xlat1.xyz / u_xlat1.www;
    u_xlat36 = _ProjectionParams.z / u_xlat1.z;
    u_xlat1.xyz = vec3(u_xlat36) * u_xlat1.xyz;
    u_xlat1.xyz = vec3(u_xlat24) * u_xlat1.xyz;
    u_xlat2.xyz = u_xlat1.yyy * hlslcc_mtx4x4unity_CameraToWorld[1].xyz;
    u_xlat1.xyw = hlslcc_mtx4x4unity_CameraToWorld[0].xyz * u_xlat1.xxx + u_xlat2.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_CameraToWorld[2].xyz * u_xlat1.zzz + u_xlat1.xyw;
    u_xlat1.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_CameraToWorld[3].xyz;
    u_xlat2.xyz = (-u_xlat1.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat36 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat36 = inversesqrt(u_xlat36);
    u_xlat2.xyz = vec3(u_xlat36) * u_xlat2.xyz;
    u_xlat36 = dot(CameraFoward.xyz, (-u_xlat2.xyz));
    u_xlat36 = _ProjectionParams.y / u_xlat36;
    u_xlat2.xyz = (-u_xlat2.xyz) * vec3(u_xlat36) + _WorldSpaceCameraPos.xyz;
    u_xlat1.xyz = _SpotLightParams.xyz * ShadowBias.xxx + u_xlat1.xyz;
    u_xlat1.xyz = (-u_xlat1.xyz) + u_xlat2.xyz;
    u_xlat36 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat36 = sqrt(u_xlat36);
    u_xlat1.xyz = u_xlat1.xyz / vec3(u_xlat36);
    u_xlat36 = min(u_xlat36, MaxRayDistance.xxyx.z);
    u_xlat37 = min(u_xlat36, MaxRayDistance.xxyx.y);
    u_xlat36 = u_xlat36 + (-u_xlat37);
    u_xlat38 = min(Density.y, 128.0);
    u_xlati3 = int(u_xlat38);
    u_xlat38 = trunc(u_xlat38);
    u_xlat15 = u_xlat37 / u_xlat38;
    u_xlat27.xy = _ScreenParams.xy * vec2(vec2(VolumeScale, VolumeScale));
    u_xlat0.xy = u_xlat0.xy * u_xlat27.xy;
    u_xlat0.xy = u_xlat0.xy / vec2(vec2(HxTileSize, HxTileSize));
    u_xlatb27.xy = greaterThanEqual(u_xlat0.xyxy, (-u_xlat0.xyxy)).xy;
    u_xlat0.xy = fract(abs(u_xlat0.xy));
    u_xlat0.x = (u_xlatb27.x) ? u_xlat0.x : (-u_xlat0.x);
    u_xlat0.y = (u_xlatb27.y) ? u_xlat0.y : (-u_xlat0.y);
    u_xlat0.xy = u_xlat0.xy * vec2(vec2(HxTileSize, HxTileSize));
    u_xlat0.xy = u_xlat0.xy / vec2(vec2(HxTileSize, HxTileSize));
    u_xlat10_4 = textureLod(Tile5x5, u_xlat0.xy, 0.0);
    u_xlat0.x = u_xlat10_4.x + hxRayOffset;
    u_xlat0.x = fract(u_xlat0.x);
    u_xlat12 = u_xlat0.x * u_xlat15;
    u_xlat2.xyz = (-vec3(u_xlat12)) * u_xlat1.xyz + u_xlat2.xyz;
    u_xlat12 = dot(_SpotLightParams.xyz, u_xlat1.xyz);
    u_xlatb24 = 0.99000001<u_xlat24;
    u_xlat24 = u_xlatb24 ? 1.0 : float(0.0);
    u_xlat27.x = (-Phase2.w) * u_xlat12 + Phase2.z;
    u_xlat27.x = log2(u_xlat27.x);
    u_xlat27.x = u_xlat27.x * 1.5;
    u_xlat27.x = exp2(u_xlat27.x);
    u_xlat27.x = max(u_xlat27.x, 9.99999975e-05);
    u_xlat27.x = Phase2.y / u_xlat27.x;
    u_xlat27.x = u_xlat27.x * Phase2.x;
    u_xlat27.x = u_xlat27.x * SunSize.x;
    u_xlat24 = max(u_xlat24, SunSize.y);
    u_xlat24 = u_xlat24 * u_xlat27.x;
    u_xlat24 = min(u_xlat24, 100.0);
    u_xlat27.x = (-Phase.w) * u_xlat12 + Phase.z;
    u_xlat27.x = log2(u_xlat27.x);
    u_xlat27.x = u_xlat27.x * 1.5;
    u_xlat27.x = exp2(u_xlat27.x);
    u_xlat27.x = Phase.y / u_xlat27.x;
    u_xlat27.x = u_xlat27.x * Phase.x;
    u_xlat24 = max(u_xlat24, u_xlat27.x);
    u_xlat12 = u_xlat12 + 1.0;
    u_xlat12 = u_xlat12 * TintPercent;
    u_xlat12 = u_xlat12 * 0.5;
    u_xlat12 = clamp(u_xlat12, 0.0, 1.0);
    u_xlat4.xyz = vec3(LightColour.x, LightColour.y, LightColour.z) + (-LightColour2.xyz);
    u_xlat4.xyz = vec3(u_xlat12) * u_xlat4.xyz + LightColour2.xyz;
    u_xlat5.xyz = u_xlat2.xyz + (-_WorldSpaceCameraPos.xyz);
    u_xlat12 = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat12 = sqrt(u_xlat12);
    u_xlat5.z = 0.0;
    u_xlat6.xyz = u_xlat2.xyz;
    u_xlat27.x = float(0.0);
    u_xlat27.y = float(0.0);
    u_xlat40 = u_xlat12;
    for(int u_xlati_loop_1 = 0 ; u_xlati_loop_1<u_xlati3 ; u_xlati_loop_1++)
    {
        u_xlatb7 = greaterThanEqual(vec4(u_xlat40), _LightSplitsNear);
        u_xlat7 = mix(vec4(0.0, 0.0, 0.0, 0.0), vec4(1.0, 1.0, 1.0, 1.0), vec4(u_xlatb7));
        u_xlatb8 = lessThan(vec4(u_xlat40), _LightSplitsFar);
        u_xlat8 = mix(vec4(0.0, 0.0, 0.0, 0.0), vec4(1.0, 1.0, 1.0, 1.0), vec4(u_xlatb8));
        u_xlat9 = u_xlat7 * u_xlat8;
        u_xlat42 = u_xlat9.y + u_xlat9.x;
        u_xlat42 = u_xlat7.z * u_xlat8.z + u_xlat42;
        u_xlat42 = u_xlat7.w * u_xlat8.w + u_xlat42;
        u_xlatb42 = 0.0<u_xlat42;
        u_xlat7.xyz = u_xlat6.yyy * hlslcc_mtx4x4unity_WorldToShadow[1].xyz;
        u_xlat7.xyz = hlslcc_mtx4x4unity_WorldToShadow[0].xyz * u_xlat6.xxx + u_xlat7.xyz;
        u_xlat7.xyz = hlslcc_mtx4x4unity_WorldToShadow[2].xyz * u_xlat6.zzz + u_xlat7.xyz;
        u_xlat7.xyz = u_xlat7.xyz + hlslcc_mtx4x4unity_WorldToShadow[3].xyz;
        u_xlat8.xyz = u_xlat6.yyy * hlslcc_mtx4x4unity_WorldToShadow[5].xyz;
        u_xlat8.xyz = hlslcc_mtx4x4unity_WorldToShadow[4].xyz * u_xlat6.xxx + u_xlat8.xyz;
        u_xlat8.xyz = hlslcc_mtx4x4unity_WorldToShadow[6].xyz * u_xlat6.zzz + u_xlat8.xyz;
        u_xlat8.xyz = u_xlat8.xyz + hlslcc_mtx4x4unity_WorldToShadow[7].xyz;
        u_xlat10.xyz = u_xlat6.yyy * hlslcc_mtx4x4unity_WorldToShadow[9].xyz;
        u_xlat10.xyz = hlslcc_mtx4x4unity_WorldToShadow[8].xyz * u_xlat6.xxx + u_xlat10.xyz;
        u_xlat10.xyz = hlslcc_mtx4x4unity_WorldToShadow[10].xyz * u_xlat6.zzz + u_xlat10.xyz;
        u_xlat10.xyz = u_xlat10.xyz + hlslcc_mtx4x4unity_WorldToShadow[11].xyz;
        u_xlat11.xyz = u_xlat6.yyy * hlslcc_mtx4x4unity_WorldToShadow[13].xyz;
        u_xlat11.xyz = hlslcc_mtx4x4unity_WorldToShadow[12].xyz * u_xlat6.xxx + u_xlat11.xyz;
        u_xlat11.xyz = hlslcc_mtx4x4unity_WorldToShadow[14].xyz * u_xlat6.zzz + u_xlat11.xyz;
        u_xlat11.xyz = u_xlat11.xyz + hlslcc_mtx4x4unity_WorldToShadow[15].xyz;
        u_xlat8.xyz = u_xlat9.yyy * u_xlat8.xyz;
        u_xlat7.xyz = u_xlat7.xyz * u_xlat9.xxx + u_xlat8.xyz;
        u_xlat7.xyz = u_xlat10.xyz * u_xlat9.zzz + u_xlat7.xyz;
        u_xlat7.xyz = u_xlat11.xyz * u_xlat9.www + u_xlat7.xyz;
        vec3 txVec0 = vec3(u_xlat7.xy,u_xlat7.z);
        u_xlat7.x = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
        u_xlat42 = (u_xlatb42) ? u_xlat7.x : 1.0;
        u_xlat7.x = Density.x;
        u_xlat7.y = float(0.0);
        u_xlat7.z = float(0.0);
        u_xlat16_43 = float(0.0);
        while(true){
            u_xlatb8.x = floatBitsToInt(u_xlat7).z>=10;
            u_xlati43 = 0;
            if(u_xlatb8.x){break;}
            u_xlatb8.x = -1.0==hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].y;
            if(u_xlatb8.x){
                u_xlat7.xy = u_xlat7.xx;
                u_xlati43 = int(0xFFFFFFFFu);
                break;
            //ENDIF
            }
            u_xlati20 = floatBitsToInt(u_xlat7).z << 2;
            u_xlat9.xyz = u_xlat6.yyy * hlslcc_mtx4x4hxVolumeMatrixOld[(u_xlati20 + 1)].xyz;
            u_xlat9.xyz = hlslcc_mtx4x4hxVolumeMatrixOld[u_xlati20].xyz * u_xlat6.xxx + u_xlat9.xyz;
            u_xlat9.xyz = hlslcc_mtx4x4hxVolumeMatrixOld[(u_xlati20 + 2)].xyz * u_xlat6.zzz + u_xlat9.xyz;
            u_xlat20.xyz = u_xlat9.xyz + hlslcc_mtx4x4hxVolumeMatrixOld[(u_xlati20 + 3)].xyz;
            u_xlatb9.xyz = lessThan(vec4(-0.5, -0.5, -0.5, 0.0), u_xlat20.xyzx).xyz;
            u_xlatb10.xyz = lessThan(u_xlat20.xyzx, vec4(0.5, 0.5, 0.5, 0.0)).xyz;
            u_xlatb9.x = u_xlatb9.x && u_xlatb10.x;
            u_xlatb9.x = u_xlatb9.y && u_xlatb9.x;
            u_xlatb9.x = u_xlatb10.y && u_xlatb9.x;
            u_xlatb9.x = u_xlatb9.z && u_xlatb9.x;
            u_xlatb9.x = u_xlatb10.z && u_xlatb9.x;
            if(u_xlatb9.x){
                u_xlatb9.x = 3.0>=hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].y;
                if(u_xlatb9.x){
                    u_xlat9.x = max(u_xlat7.x, hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x);
                    u_xlat21 = u_xlat7.x + hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x;
                    u_xlat33 = min(u_xlat7.x, hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x);
                    u_xlatb10 = equal(vec4(0.0, 1.0, 2.0, 3.0), hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].yyyy);
                    u_xlat45 = u_xlat7.x + (-hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x);
                    u_xlat45 = (u_xlatb10.w) ? u_xlat45 : u_xlat7.x;
                    u_xlat33 = (u_xlatb10.z) ? u_xlat33 : u_xlat45;
                    u_xlat21 = (u_xlatb10.y) ? u_xlat21 : u_xlat33;
                    u_xlat5.x = (u_xlatb10.x) ? u_xlat9.x : u_xlat21;
                } else {
                    u_xlatb9.x = 7.0>=hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].y;
                    u_xlat32 = dot(u_xlat20.xyz, u_xlat20.xyz);
                    u_xlat32 = sqrt(u_xlat32);
                    u_xlatb32 = u_xlat32<0.5;
                    u_xlatb32 = u_xlatb32 && u_xlatb9.x;
                    if(u_xlatb32){
                        u_xlat32 = max(u_xlat7.x, hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x);
                        u_xlat9.x = u_xlat7.x + hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x;
                        u_xlat21 = min(u_xlat7.x, hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x);
                        u_xlatb10 = equal(vec4(4.0, 5.0, 6.0, 7.0), hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].yyyy);
                        u_xlat33 = u_xlat7.x + (-hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x);
                        u_xlat33 = (u_xlatb10.w) ? u_xlat33 : u_xlat7.x;
                        u_xlat21 = (u_xlatb10.z) ? u_xlat21 : u_xlat33;
                        u_xlat9.x = (u_xlatb10.y) ? u_xlat9.x : u_xlat21;
                        u_xlat5.x = (u_xlatb10.x) ? u_xlat32 : u_xlat9.x;
                    } else {
                        u_xlatb32 = 11.0>=hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].y;
                        u_xlat20.x = dot(u_xlat20.xz, u_xlat20.xz);
                        u_xlat20.x = sqrt(u_xlat20.x);
                        u_xlatb20 = u_xlat20.x<0.5;
                        u_xlatb20 = u_xlatb20 && u_xlatb32;
                        u_xlat32 = max(u_xlat7.x, hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x);
                        u_xlat44 = u_xlat7.x + hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x;
                        u_xlat9.x = min(u_xlat7.x, hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x);
                        u_xlatb10 = equal(vec4(8.0, 9.0, 10.0, 11.0), hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].yyyy);
                        u_xlat21 = u_xlat7.x + (-hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x);
                        u_xlat21 = (u_xlatb10.w) ? u_xlat21 : u_xlat7.x;
                        u_xlat9.x = (u_xlatb10.z) ? u_xlat9.x : u_xlat21;
                        u_xlat44 = (u_xlatb10.y) ? u_xlat44 : u_xlat9.x;
                        u_xlat32 = (u_xlatb10.x) ? u_xlat32 : u_xlat44;
                        u_xlat5.x = (u_xlatb20) ? u_xlat32 : u_xlat7.x;
                    //ENDIF
                    }
                //ENDIF
                }
            } else {
                u_xlat5.x = u_xlat7.x;
            //ENDIF
            }
            u_xlat5.y = intBitsToFloat(floatBitsToInt(u_xlat7).z + 1);
            u_xlat7.xyz = u_xlat5.xzy;
            u_xlatb43 = u_xlatb8.x;
        }
        u_xlat5.x = (u_xlati43 != 0) ? u_xlat7.y : u_xlat7.x;
        u_xlat17 = u_xlat5.x * Density.w;
        u_xlat27.y = u_xlat17 * u_xlat15 + u_xlat27.y;
        u_xlat17 = u_xlat27.y * -1.44269502;
        u_xlat17 = exp2(u_xlat17);
        u_xlat5.x = u_xlat15 * u_xlat5.x;
        u_xlat5.x = u_xlat17 * u_xlat5.x;
        u_xlat17 = (-u_xlat42) + 1.0;
        u_xlat17 = u_xlat17 * ShadowBias.z;
        u_xlat17 = u_xlat42 * u_xlat24 + u_xlat17;
        u_xlat5.x = u_xlat5.x * u_xlat17;
        u_xlat5.x = max(u_xlat5.x, 0.0);
        u_xlat27.x = u_xlat27.x + u_xlat5.x;
        u_xlat6.xyz = (-u_xlat1.xyz) * vec3(u_xlat15) + u_xlat6.xyz;
        u_xlat40 = u_xlat15 + u_xlat40;
    }
    u_xlatb12 = 0.0<u_xlat36;
    if(u_xlatb12){
        u_xlat12 = u_xlat37 + u_xlat15;
        u_xlat2.xyz = (-u_xlat1.xyz) * vec3(u_xlat12) + _WorldSpaceCameraPos.xyz;
        u_xlat12 = u_xlat36 / u_xlat38;
        u_xlat0.x = u_xlat0.x * u_xlat12;
        u_xlat2.xyz = (-u_xlat0.xxx) * u_xlat1.xyz + u_xlat2.xyz;
        u_xlat5.z = 0.0;
        u_xlat6.xyz = u_xlat2.xyz;
        u_xlat0.xw = u_xlat27.xy;
        for(int u_xlati_loop_2 = 0 ; u_xlati_loop_2<u_xlati3 ; u_xlati_loop_2++)
        {
            u_xlat7.x = Density.x;
            u_xlat7.y = float(0.0);
            u_xlat7.z = float(0.0);
            u_xlat16_38 = 0.0;
            while(true){
                u_xlatb15 = floatBitsToInt(u_xlat7).z>=10;
                u_xlati38 = 0;
                if(u_xlatb15){break;}
                u_xlatb15 = -1.0==hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].y;
                if(u_xlatb15){
                    u_xlat7.xy = u_xlat7.xx;
                    u_xlati38 = int(0xFFFFFFFFu);
                    break;
                //ENDIF
                }
                u_xlati40 = floatBitsToInt(u_xlat7).z << 2;
                u_xlat8.xyz = u_xlat6.yyy * hlslcc_mtx4x4hxVolumeMatrixOld[(u_xlati40 + 1)].xyz;
                u_xlat8.xyz = hlslcc_mtx4x4hxVolumeMatrixOld[u_xlati40].xyz * u_xlat6.xxx + u_xlat8.xyz;
                u_xlat8.xyz = hlslcc_mtx4x4hxVolumeMatrixOld[(u_xlati40 + 2)].xyz * u_xlat6.zzz + u_xlat8.xyz;
                u_xlat8.xyz = u_xlat8.xyz + hlslcc_mtx4x4hxVolumeMatrixOld[(u_xlati40 + 3)].xyz;
                u_xlatb9.xyz = lessThan(vec4(-0.5, -0.5, -0.5, 0.0), u_xlat8.xyzx).xyz;
                u_xlatb10.xyz = lessThan(u_xlat8.xyzx, vec4(0.5, 0.5, 0.5, 0.0)).xyz;
                u_xlatb40 = u_xlatb9.x && u_xlatb10.x;
                u_xlatb40 = u_xlatb9.y && u_xlatb40;
                u_xlatb40 = u_xlatb10.y && u_xlatb40;
                u_xlatb40 = u_xlatb9.z && u_xlatb40;
                u_xlatb40 = u_xlatb10.z && u_xlatb40;
                if(u_xlatb40){
                    u_xlatb40 = 3.0>=hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].y;
                    if(u_xlatb40){
                        u_xlat40 = max(u_xlat7.x, hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x);
                        u_xlat41 = u_xlat7.x + hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x;
                        u_xlat42 = min(u_xlat7.x, hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x);
                        u_xlatb9 = equal(vec4(0.0, 1.0, 2.0, 3.0), hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].yyyy);
                        u_xlat43 = u_xlat7.x + (-hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x);
                        u_xlat43 = (u_xlatb9.w) ? u_xlat43 : u_xlat7.x;
                        u_xlat42 = (u_xlatb9.z) ? u_xlat42 : u_xlat43;
                        u_xlat41 = (u_xlatb9.y) ? u_xlat41 : u_xlat42;
                        u_xlat5.x = (u_xlatb9.x) ? u_xlat40 : u_xlat41;
                    } else {
                        u_xlatb40 = 7.0>=hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].y;
                        u_xlat41 = dot(u_xlat8.xyz, u_xlat8.xyz);
                        u_xlat41 = sqrt(u_xlat41);
                        u_xlatb41 = u_xlat41<0.5;
                        u_xlatb40 = u_xlatb40 && u_xlatb41;
                        if(u_xlatb40){
                            u_xlat40 = max(u_xlat7.x, hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x);
                            u_xlat41 = u_xlat7.x + hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x;
                            u_xlat42 = min(u_xlat7.x, hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x);
                            u_xlatb9 = equal(vec4(4.0, 5.0, 6.0, 7.0), hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].yyyy);
                            u_xlat43 = u_xlat7.x + (-hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x);
                            u_xlat43 = (u_xlatb9.w) ? u_xlat43 : u_xlat7.x;
                            u_xlat42 = (u_xlatb9.z) ? u_xlat42 : u_xlat43;
                            u_xlat41 = (u_xlatb9.y) ? u_xlat41 : u_xlat42;
                            u_xlat5.x = (u_xlatb9.x) ? u_xlat40 : u_xlat41;
                        } else {
                            u_xlatb40 = 11.0>=hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].y;
                            u_xlat41 = dot(u_xlat8.xz, u_xlat8.xz);
                            u_xlat41 = sqrt(u_xlat41);
                            u_xlatb41 = u_xlat41<0.5;
                            u_xlatb40 = u_xlatb40 && u_xlatb41;
                            u_xlat41 = max(u_xlat7.x, hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x);
                            u_xlat42 = u_xlat7.x + hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x;
                            u_xlat43 = min(u_xlat7.x, hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x);
                            u_xlatb8 = equal(vec4(8.0, 9.0, 10.0, 11.0), hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].yyyy);
                            u_xlat9.x = u_xlat7.x + (-hxVolumeSettingsOld[floatBitsToInt(u_xlat7).z].x);
                            u_xlat44 = (u_xlatb8.w) ? u_xlat9.x : u_xlat7.x;
                            u_xlat43 = (u_xlatb8.z) ? u_xlat43 : u_xlat44;
                            u_xlat42 = (u_xlatb8.y) ? u_xlat42 : u_xlat43;
                            u_xlat41 = (u_xlatb8.x) ? u_xlat41 : u_xlat42;
                            u_xlat5.x = (u_xlatb40) ? u_xlat41 : u_xlat7.x;
                        //ENDIF
                        }
                    //ENDIF
                    }
                } else {
                    u_xlat5.x = u_xlat7.x;
                //ENDIF
                }
                u_xlat5.y = intBitsToFloat(floatBitsToInt(u_xlat7).z + 1);
                u_xlat7.xyz = u_xlat5.xzy;
                u_xlatb38 = u_xlatb15;
            }
            u_xlat15 = (u_xlati38 != 0) ? u_xlat7.y : u_xlat7.x;
            u_xlat40 = u_xlat15 * Density.w;
            u_xlat0.w = u_xlat40 * u_xlat12 + u_xlat0.w;
            u_xlat40 = u_xlat0.w * -1.44269502;
            u_xlat40 = exp2(u_xlat40);
            u_xlat15 = u_xlat12 * u_xlat15;
            u_xlat15 = u_xlat40 * u_xlat15;
            u_xlat15 = u_xlat24 * u_xlat15;
            u_xlat15 = max(u_xlat15, 0.0);
            u_xlat0.x = u_xlat0.x + u_xlat15;
            u_xlat6.xyz = (-u_xlat1.xyz) * vec3(u_xlat12) + u_xlat6.xyz;
        }
        u_xlat27.xy = u_xlat0.xw;
    //ENDIF
    }
    u_xlat0.x = u_xlat27.y * -1.44269502;
    u_xlat0.x = exp2(u_xlat0.x);
    u_xlat0.x = min(u_xlat0.x, 1.0);
    SV_Target0.xyz = u_xlat27.xxx * u_xlat4.xyz;
    u_xlat12 = (-ExtinctionEffect) + 1.0;
    u_xlat24 = (-u_xlat0.x) + 1.0;
    u_xlat0.x = u_xlat12 * u_xlat24 + u_xlat0.x;
    u_xlat0.x = (-u_xlat0.x) + 1.0;
    SV_Target0.w = u_xlat0.x * FirstLight;
    return;
}

#endif
                               